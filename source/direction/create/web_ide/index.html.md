---
layout: markdown_page
title: "Category Strategy - Web IDE"
---

- TOC
{:toc}

## Web IDE

| Stage | Maturity |
| --- | --- |
| [Create](/direction/dev/#create) | [Viable](/direction/maturity/) |

### Introduction and how you can help
Thanks for visiting this category page on the Web IDE in GitLab. This page belongs to the [Editor](/handbook/product/categories/#editor-group) group of the Create stage and is maintained by Kai Armstrong([E-Mail](mailto:karmstrong@gitlab.com)).

This strategy is constantly evolving and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/gitlab-org/gitlab/issues?label_name%5B%5D=Category%3AWeb+IDE) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=Category%3AWeb%20IDE) on this page. Sharing your feedback directly on GitLab.com  or submitting a Merge Request to this page are the best ways to contribute to our strategy.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of your need for the Web IDE, we'd especially love to hear from you.

### Overview
Building new features involves more than the just primary code development process. It may include things like Code Review, documentation or simple bug fixes. We envision the Web IDE becoming the primary tool for these secondary engineering efforts.

These tasks are essential parts of the engineering process, but can often be disruptive to local environments and have challenges when working inside of GitLab. Currently GitLab supports editing multiple files within a single interface, but editing files alone isn't enough to support these efforts. This makes it harder to resolve feedback in merge requests, fix small bugs when reading source code, or contribute to projects.

Development environments are also a very personal tool of engineers and customization is an important part of that. Engineers working on teams need to standardize on coding standards and styles in all of the editing environments they use. Configuration and customization are important features to make the Web IDE a valueable tool for developers.

We want to make it easier for engineers to work on secondary development items in the Web IDE. By removing barriers to the code review process, working on standardizing configuration and other support tooling engineers will be able to provide more meaningful feedback in a GitLab native environment.

### Target Audience and Experience
<!-- An overview of the personas (https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas#user-personas) involved in this category. An overview 
of the evolving user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels. -->
The Web IDE is primarily targeted at more **technical personas** working on **secondary development tasks** (e.g. code review, docs, and small fixes) who are familiar with working in local development environments. GitLab personas that the Web IDE is seeking to solve for are:
 - [Sasha (Software Developer)](/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer)
 - [Devon (DevOps Engineer)](/handbook/marketing/product-marketing/roles-personas/#devon-devops-engineer)

By prioritizing the needs of engineering users for non-primary tasks GitLab can focus on providing an experience that supports the ability to contribute in a cloud environment.

While we're prioritizing the needs of engineering users first, the editor group is also supportive of other editing personas and existing editing use cases within GitLab. Additional improvements will be addressed via [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AWeb%20IDE) and in other areas like the [Static Site Editor](https://about.gitlab.com/direction/create/static_site_editor/).

#### Challenges to address
<!-- 
- What needs, goals, or jobs to be done do the users have?
- How do users address these challenges today? What products or work-arounds are utilized?

Provide links to UX Research issues, which validate these problems exist.
-->
Engineers generally hold very specific editing preferences and engrained workflows. Supporting user preference and configuration in the Web IDE will be important in garnering adoption for the Web IDE. It will also be important to introduce value added workflows that don't try to replace primary engineering activities.

### Where we are Headed
<!-- Describe the future state for your category. What problems will you solve?
What will the category look like once you've achieved your strategy? Use narrative
techniques to paint a picture of how the lives of your users will benefit from using this
category once your strategy is fully realized -->
Code review within GitLab can be a burdensome process for some users that requires reviewers to checkout branches locally for review and then return to the GitLab UI to find the line and file and provide comments. When the feedback is reviewed, engineers must examine feedback in one window and then return to their editor to make changes. By expanding code review tools in to the Web IDE we can speed up and make that review process easier.

Documentation is a less intensive type of engineering effort that requires fewer tools than traditional development environments. Enabling technical users to produce documentation in the Web IDE easier and more quickly will be important as we continue to expand the Web IDE. This includes enhancing markdown tooling and providing realtime collaborative tools for editing.

Editor [configuration and customization](https://www.cmcrossroads.com/article/beware-ide-risks-standardizing-one-ide) are important parts of IDE experiences. Teams working together often standardize editor configurations through the use of an `.editorconfig` file and developers customize other aspects of the editor to their own preference. Working to support these will make the Web IDE feel more like home for developers for all their tasks.

Other areas of interest include supporting remote development environments and integrations with local IDEs for more seamless GitLab experiences. Providing first class support for editing specialized GitLab files like [`.gitlab-ci.yml`](https://gitlab.com/gitlab-org/gitlab/issues/38033) file, issue templates]and [CODEOWNDERS](https://gitlab.com/gitlab-org/gitlab/issues/38224) are important.

### Maturity

Currently, GitLab's maturity in the Web IDE is *viable*. Here's why:

 - GitLab currently offers a rich editing experience via the Web IDE. However, the Web IDE does not currently support meaningful development or contribute to secondary engineering activities effectively. Without these features the Web IDE cannot be effectively used to support engineers.

A **[complete](https://gitlab.com/groups/gitlab-org/-/epics/1469)** Web IDE category ensures that the Web IDE is used for all file/code editing in GitLab. Moving beyond using the Web IDE for all editing the focus will be on standardization of editing environments through the use of `.editorconfig` and support for user syntax highlighting (including dark mode). Finally, the Web IDE must provide some support for things like linting and code completion.

Part of a **[Lovable](https://gitlab.com/groups/gitlab-org/-/epics/1470)** Web IDE comes by providing support for the Code Review process inside of the Web IDE. This involves support for both leaving and responding to feedback inside of the Web IDE. The Web IDE should also include code navigation features like `Jump to Definition` and `Find References` to better support this process.

### What's Next & Why
**In progress:** Self-hosted client side evaluation [&484](https://gitlab.com/groups/gitlab-org/-/epics/484)

Client-side evaluation live preview in the Web IDE is powered by Codesandbox and currently relies on Codesandbox services to retrieve and package dependencies. Ideally, self hosted GitLab installations should be able to use Codesandbox capabilities in an entirely self hosted manner.

**Next:** Use Web IDE for all file/code editing [&498](https://gitlab.com/groups/gitlab-org/-/epics/498)

Standardizing on a single editor experience supported by our use of Monaco will allow us to bring a consistent experience to users in both the Web IDE and single file editing modes like Snippets or GitLab CI.

**Future:** Increase configurability of Web IDE [&175](https://gitlab.com/groups/gitlab-org/-/epics/175)

User preference and organization standardization are important aspects of the editing experience. We'll look to support user syntax highlighting preferences (inlcuding dark mode), editorconfig standards and other important preferences for user customization.

**Future:** Lint, Format and Code Completion [&70](https://gitlab.com/groups/gitlab-org/-/epics/70)

As users continue to move more work to the Web IDE tools commonly found in local environments become more important. By bringing linting, formatters and code completion to the Web IDE users can be more confident in their work and spend less time in code reviews working through simple errors and poor formating.

### What is Not Planned Right Now
<!-- Often it's just as important to talk about what you're not doing as it is to 
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand
the reasoning-->

We're not currently focused on solving the needs of non-engineering based personas with the Web IDE. Non-engineers require a different type of editing experience and that can largely be focused around different types of content. These editing experiences may include documentation or notes based in Markdown, or more WYSIWYG experiences related to content management systems. The [Static Site Editor group](https://about.gitlab.com/handbook/product/categories/#static-site-editor-group) will be more focused on these efforts.

### Competitive Landscape
- [Cloud9](https://aws.amazon.com/cloud9/)
- [Codesandbox](https://codesandbox.io/)
- [Repl.it](https://repl.it/)
- [Koding](https://www.koding.com/)
- [StackBlitz](https://stackblitz.com/)
- [Theia](https://www.theia-ide.org/)
- [Gitpod](https://www.gitpod.io/)
- [Coder](https://coder.com/)
- [Visual Studio Online](https://online.visualstudio.com/)

### Analyst Landscape

Currently the Analyst landscape is fragmented on cloud native development experiences. This market will continue to mature as Microsoft pushes ahead with VS Code Online and Amazon further integrates Cloud9 in to their offerings.

[https://www.g2crowd.com/categories/integrated-development-environment-ide](https://www.g2crowd.com/categories/integrated-development-environment-ide)
[https://www.theserverside.com/news/450433105/AWS-Cloud9-IDE-threatens-Microsoft-developer-base](https://www.theserverside.com/news/450433105/AWS-Cloud9-IDE-threatens-Microsoft-developer-base)


<!-- ### Top Customer Success/Sales issue(s) -->
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

### Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

Top user issues relate to the configurability and customization of the Web IDE. These focus on items like standardizing editor coding style through configuration files and supporting user customization for themes and other preferences.

 - Increase configurability of Web IDE [&175](https://gitlab.com/groups/gitlab-org/-/epics/175)

### Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

GitLab's current internal usage of the Web IDE is very heavily focused around non-technical personas and so there are not any current issues. As we look towards delivering on an IDE for technical users we'll update this section as appropriate.

### Top Vision Item(s)
<!-- What's the most important thing to move your vision forward?-->

The most important items to moving the Web IDE strategy forward is Self-hosted client side evaluation [&484](https://gitlab.com/groups/gitlab-org/-/epics/484). By enabling all GitLab instances to run client side evaluation in the browser more developers can contribute to these types of projects without the constraints of local dependencies.
