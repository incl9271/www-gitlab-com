---
layout: markdown_page
title: "Category Vision - Runner"
---

- TOC
{:toc}

## GitLab Runner

The GitLab Runner is our execution agent that works with [GitLab CI](/direction/verify/continuous_integration)
to execute the jobs in your pipelines.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ARunner)
- [Overall Vision](/direction/verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1546) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

### The Custom Executor

A common request we get is to add support for various platforms to the Runner. We've chosen to support
the plethora of different systems out there by recently adding support for a [custom executor](https://docs.gitlab.com/runner/executors/custom.html)
that can be overridden to support different needs. In this way platforms like Lambda, z/OS, vSphere, and
even custom in-house implementations can be supported in the runner for your needs.

## What's Next & Why

Up next is the beta launch of [Windows Shared Runners](https://gitlab.com/gitlab-org/gitlab-runner/issues/4815) on GitLab.com  This will gives users the tooling to build their Windows projects on GitLab.com without needing to setup and configure their own Windows build machines.

## Maturity Plan

Since this category is already at the "Lovable" maturity level (see our
[definitions of maturity levels](/direction/maturity/)),
we do not have an upcoming target for bringing it to the next level. It's important to us that
we defend our lovable status, though, so if you see any actual or potential gap please let us know in
our [public epic](https://gitlab.com/groups/gitlab-org/-/epics/1546) for this category.

Maturing the Windows executor is of particular importance as we improve our support for Windows
across the board. There are a few issues we've identified as being critical to that effort:

- [Support named pipes for Windows Docker executor](https://gitlab.com/gitlab-org/gitlab-runner/issues/4295)
- [Support services on Windows Docker containers](https://gitlab.com/gitlab-org/gitlab-runner/issues/4186)
- [Use Docker Save to publish docker helper images](https://gitlab.com/gitlab-org/gitlab-runner/issues/3979)
- [Distribute Docker windows image for gitlab-runner](https://gitlab.com/gitlab-org/gitlab-runner/issues/3914)
- [Add support for Windows device for Docker executor](https://gitlab.com/gitlab-org/gitlab-runner/issues/3923)

## Competitive Landscape

For the moment, the Runner is evaluated as part of the comprehensive competitive analysis in the [Continuous Integration category](/direction/verify/continuous_integration/#competitive-landscape)

## Top Customer Success/Sales Issue(s)

For the CS team, the issue [gitlab-runner#3121](https://gitlab.com/gitlab-org/gitlab-runner/issues/3121) where orphaned
processes can cause issues with the runner in certain cases has been highlighted as generating
support issues.

## Top Customer Issue(s)

We have a few top issues that we're investigating, but a couple key highlights are exposing the `image` keyword to the custom executor to allow for more powerful executors ([gitlab-runner#4357](https://gitlab.com/gitlab-org/gitlab-runner/issues/4357)) and support for `raw` variables ([gitlab-runner#4696](https://gitlab.com/gitlab-org/gitlab-runner/issues/4696)) which will improve the ability for GitLab to handle variable expansion.

Additionally, although it is not implemented entirely by the Runner team, implementing Vault integration support in GitLab via [gitlab#28321](https://gitlab.com/gitlab-org/gitlab/issues/28321) is important.

Other popular issues include:

- [gitlab-runner#2797](https://gitlab.com/gitlab-org/gitlab-runner/issues/2797): Local runner execution
- [gitlab-runner#2229](https://gitlab.com/gitlab-org/gitlab-runner/issues/2229): Adding Services With Kubernetes Executor
- [gitlab-runner#2007](https://gitlab.com/gitlab-org/gitlab-runner/issues/2007): Variables nested substitution
- [gitlab-runner#1736](https://gitlab.com/gitlab-org/gitlab-runner/issues/1736): File/directory creation umask when cloning is `0000`
- [gitlab-runner#4518](https://gitlab.com/gitlab-org/gitlab-runner/issues/4518): Kubernetes Runner allow for container security context

## Top Internal Customer Issue(s)

There are a few top internal customer issues that we're investigating :

- [gitlab-runner#1042](https://gitlab.com/gitlab-org/gitlab-runner/issues/1042): Create network per build to link containers together
- [gitlab#25969](https://gitlab.com/gitlab-org/gitlab/issues/25969): Allow advanced configuration of GitLab runner when installing GitLab managed apps

## Top Vision Item(s)

### Runner Performance/Availability

As discussed on our [CI/CD vision page](https://about.gitlab.com/direction/cicd/#speedy-reliable-pipelines), one of the themes in support of our long term vision and strategy is to provide speedy, reliable pipelines. A vital enabler of that goal is preventing hourly runner queue time spikes: [gitlab-runner#7814.](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/7814) We have also set an [FY20-Q4 OKR](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5341) goal to get CI queue times under 1 minute. In addition, we will continue to focus on investigating and providing improvements for other Runner related performance issues to ensure that we are continuously delivering the performance that you need for your build jobs.

Also related to slowness is the important issue [gitlab-runner#4513](https://gitlab.com/gitlab-org/gitlab-runner/issues/4513) which tracks general improvement to execution time.

We are also considering making the shared Runner fleet available for self-managed users via [gitlab-org&835](https://gitlab.com/groups/gitlab-org/-/epics/835)

### Additional Platforms

After supporting Windows runners, we plan to add support for Mac via [gitlab-runner#4496](https://gitlab.com/gitlab-org/gitlab-runner/issues/4496) (note that this issue is temporarily confidential due to third-party involvement, but per GitLab values we're working to open it up as soon as possble.)

Supporting GitLab runners on IBM z/OS for IBM mainframes, [gitlab-runner#3263](https://gitlab.com/gitlab-org/gitlab-runner/issues/3263) is another issue that is gaining interest from the community. If this is of interest to you, please head over and join the conversation as we would love to get your feedback on this important vision item.

Other platforms of interest we're tracking, through support via the custom executor, include AWS Fargate ([gitlab-runner#2792](https://gitlab.com/gitlab-org/gitlab-runner/issues/2972), ARM ([gitlab-runner#2076](https://gitlab.com/gitlab-org/gitlab-runner/issues/2076) compatibility, and [GitLab Runners on Google Cloud Run](https://gitlab.com/gitlab-org/gitlab-runner/issues/5028).

These additional platforms are primarily being implemented through community contributions, so if you're interested in contributing to GitLab these issues are great ones to get involved with.
